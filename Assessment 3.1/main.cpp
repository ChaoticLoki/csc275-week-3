#include <iostream>
#include <bitset>
#include <string>

//Print out number as bits
void printBits(int number)
{
	std::cout << std::bitset<32>(number).to_string() << std::endl;
}

//Bitshift operation
int power2(int number, int pow)
{
	return number << pow;
}

int main()
{
	int number, pow;

	//Get user Input
	std::cout << "Enter Number: ";
	std::cin >> number;
	std::cout << "Enter Power: ";
	std::cin >> pow;

	//Print number in binary prior to calculation
	std::cout << "--==Binary Representation of Input==--" << std::endl;
	printBits(number);
	int result = power2(number,pow);
	std::cout << "--==Binary Representation of Result==--" << std::endl;
	//Print number in binary after calculation
	printBits(result);
	std::cout << "Result: " << result << std::endl;
	std::cin >> number;
}