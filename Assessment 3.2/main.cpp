#include <string>
#include <iostream>
#include <stack>

void printStack(std::stack<int> stack)
{
	std::cout << "--==Current Stack==--" << std::endl;
	for (std::stack<int> dump = stack; !dump.empty(); dump.pop())
		std::cout << dump.top() << std::endl;
}

void postfix(std::string input)
{
	//Variables used by function
	std::stack<int> stack;
	int x, y, result;
	//
	int index = 0;
	while (index < input.length())
	{
		//Ignore Spaces
		if(isspace(input[index]))
		{
			index++;
			continue;
		}
		//If it is a number push it onto the stack
		else if(isdigit(input[index]))
		{
			//handle number higher than 9
			int num = 0;
			do
			{
				num = num*10 + (input[index]-'0');
				index++;
			}while(isdigit(input[index]));
			stack.push(num);
		}
		else
		{
			//current index points to an operand.
			//Get the top two numbers from stack
			y = stack.top();
			stack.pop();
			x = stack.top();
			stack.pop();
			//check what the operand is and apply appropriate calculation.
			//then push it to the stack
			switch(input[index])
			{
				case '+':
					result = x + y;
					stack.push(result);
					break;
				case '-':
					result = x - y;
					stack.push(result);
					break;
				case '*':
					result = x * y;
					stack.push(result);
					break;
				case '/':
					result = x / y;
					stack.push(result);
					break;
			}
		}
		//increment the index to the next element in user input
		index++;
		//Print the current stack.
		printStack(stack);
	}

	//Display the result.
	std::cout << "Result: " << result << std::endl;
	std::cin >> result;
}

int main()
{
	std::string input;
	//Get User Input
	std::cout << "Enter a postfix expression: ";
	std::getline(std::cin, input);
	//Run postfix function
	postfix(input);
}